/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useState } from 'react';
import SplashScreen from 'react-native-splash-screen'
import Providers from './src/navigation';
import {
  View,
} from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import messaging from '@react-native-firebase/messaging';
import { launchPushNotification } from './src/services/LocalPushController';
import AnicureText from './src/components/AnicureText';
import { APP_GREEN } from './src/utils/constant';


const App = () => {
  const [loading, setLoading] = useState(true);
  const [isConnected, setIsConnected] = useState(true);

  useEffect(() => {
    SplashScreen.hide();
    messaging().setBackgroundMessageHandler(async (remoteMessage: any) => {
      console.log('Listening in Background ...', remoteMessage)
      launchPushNotification(remoteMessage)
    })

    messaging()
      .getInitialNotification()
      .then((remoteMessage: any) => {
        if (remoteMessage) {
          console.log(
            'Notification caused app to open from quit state:',
            remoteMessage.notification,
          );
        }
        setLoading(false);
      });
    return unsubscribe();
  }, [])


  const unsubscribe = NetInfo.addEventListener((state: any) => {
    if (isConnected !== state.isConnected) {
      setIsConnected(state.isConnected)
    }
  });


  if (loading) {
    return null;
  }

  return (
    <View style={{ flex: 1, backgroundColor: "#F4F4F4" }}>
      { isConnected === false && <AnicureText text={'Internet connectivity issues'} type="subTitle" otherStyles={{ marginTop: 0, paddingTop: 10, backgroundColor: APP_GREEN, color: "#FFFFFF", paddingVertical: 5, fontSize: 10 }} />}
      {/* @ts-ignore */}
      <Providers />
    </View>
  );
};


export default App;
