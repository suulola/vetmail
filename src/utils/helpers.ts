import dayjs from "dayjs";

export const wait = (timeout: number) => {
    return new Promise(resolve => setTimeout(resolve, timeout));
}

export const logError = (
    error: any, 
    setGeneralError: (item: any) => void, 
    setIsLoading: (state: boolean) => void,
    errorMessage?: string,
    ) => {
    console.log(error, "error catching block")
    // console.log(error.data, "222222")
    setIsLoading(false)
    setGeneralError(error?.message ?? error?.data?.message ?? errorMessage ?? "Network Error. Please try again");
}

export const minuteSecond = (date?: string): string => {
    return dayjs(date ?? new Date().toLocaleString()).format('h:mmA')
 }
export const minuteSecondDay = (date?: string): string => {
    return dayjs(date ?? new Date().toLocaleString()).format('h:mmA DD/MM')
 }
export const minuteSecondDayMonthYear = (date?: string): string => {
    return dayjs(date ?? new Date().toLocaleString()).format('h:mmA DD/MMM/YY')
 }
 export const monthDayYear = (date?: string): string => {
    return dayjs(date ?? new Date().toLocaleString()).format('MMM. DD, YYYY')
 }
 export const monthDay = (date?: string): string => {
    return dayjs(date ?? new Date().toLocaleString()).format('MMM DD')
 }
 