import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from '../screens/public/LoginScreen';
import AppIntro from '../screens/public/AppIntro';
import CreateDoctor from '../screens/public/CreateDoctor';
import AddDoctorProfessionalDetail from '../screens/public/AddDoctorProfessionalDetail';
import ConsultationDays from '../screens/public/ConsultationDays';

const Stack = createStackNavigator();

const AuthStack = () => {
  return (
      <Stack.Navigator initialRouteName="Intro" headerMode="none">
        <Stack.Screen name="Intro" component={AppIntro} />
        <Stack.Screen name="Login" component={LoginScreen} />
        <Stack.Screen name="CreateDoctor" component={CreateDoctor} />
        <Stack.Screen name="AddDoctorProfessionalDetail" component={AddDoctorProfessionalDetail} />
        <Stack.Screen name="ConsultationDays" component={ConsultationDays} />
      </Stack.Navigator>
  );
};

export default AuthStack;
