import React, { useEffect } from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { connect } from 'react-redux';

import messaging from '@react-native-firebase/messaging';


import HomeScreen from '../screens/tabs/HomeScreen';
import ChatScreen from '../screens/protected/Messaging/ChatScreen';
import TabBar from './TabBar';
import Report from '../screens/tabs/Report';
import ProfileScreen from '../screens/tabs/ProfileScreen';
import ChatHome from '../screens/protected/Messaging/ChatHome';
import Booking from '../screens/protected/Booking/Booking';
import ViewReport from '../screens/protected/Report/ViewReport';
import ViewBooking from '../screens/protected/Booking/ViewBooking';
import VideoCall from '../screens/protected/Messaging/VideoCall';
import CreateDoctor from '../screens/public/CreateDoctor';
import AddDoctorProfessionalDetail from '../screens/public/AddDoctorProfessionalDetail';
import ConsultationDays from '../screens/public/ConsultationDays';
import ReportDetails from '../screens/protected/Report/ReportDetails';
import Events from '../screens/protected/Events/Events';
import apiFetch from '../utils/apiFetch';

import { navigationRef } from './RootNavigation';
import BankInformation from '../screens/protected/Payment/BankInformation';


const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

const saveTokenToDatabase = async (token, mobileNumber) => {
  try {
   await apiFetch.post("doctor/token", { fcmToken: token, mobileNumber });
  } catch (error) {
    console.log(error)
  }
}

const TabNavigator = () => {
  
  return (
    <Tab.Navigator
      tabBar={props => <TabBar {...props} />}
      initialRouteName="Home">
      <Tab.Screen name="Home" component={HomeScreen} />
      <Tab.Screen name="Report" component={Report} />
      <Tab.Screen name="Profile" component={ProfileScreen} />
    </Tab.Navigator>
  )
}

const UserTab = ({ mobileNumber }) => {
  useEffect(() => {
    // Get the device token
    messaging()
      .getToken()
      .then(token => {
        return saveTokenToDatabase(token, mobileNumber);
      });

    return messaging().onTokenRefresh(token => {
      saveTokenToDatabase(token, mobileNumber);
    });
  }, []);

  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator initialRouteName="Landing" headerMode="none">
        <Stack.Screen name="Landing" component={TabNavigator} />
        <Stack.Screen name="ChatHome" component={ChatHome} />
        <Stack.Screen name="Chat" component={ChatScreen} />
        <Stack.Screen name="Booking" component={Booking} />
        <Stack.Screen name="ViewReport" component={ViewReport} />
        <Stack.Screen name="ViewBooking" component={ViewBooking} />
        <Stack.Screen name="VideoCall" component={VideoCall} />
        <Stack.Screen name="ReportDetail" component={ReportDetails} />

        <Stack.Screen name="CreateDoctor" component={CreateDoctor} />
        <Stack.Screen name="AddDoctorProfessionalDetail" component={AddDoctorProfessionalDetail} />
        <Stack.Screen name="ConsultationDays" component={ConsultationDays} />
        <Stack.Screen name="BankInformation" component={BankInformation} />
        
        <Stack.Screen name="Events" component={Events} />

      </Stack.Navigator>
    </NavigationContainer>
  );
};

const mapStateToProps = (state) => ({
  mobileNumber: state.user.userDetail.mobileNumber,
})


export default connect(mapStateToProps)(UserTab);
