import React, { useState } from 'react'
import { StyleSheet, SafeAreaView, ScrollView, ActivityIndicator, ToastAndroid } from 'react-native'
import { connect } from 'react-redux';
import AnicureButton from '../../../components/AnicureButton';
import AnicureImage from '../../../components/AnicureImage';
import AnicureText from '../../../components/AnicureText';
import Appbar from '../../../components/Appbar';
import FormInput from '../../../components/FormInput';
import apiFetch from '../../../utils/apiFetch';
import { APP_GREEN } from '../../../utils/constant';
import { updateUserDetail } from '../../../store/actions/userAction';
import { generalTextFieldValidation } from '../../../utils/validation';


const BankInformation = ({ navigation, mobileNumber, paymentDetails, updateUserDetail }: any) => {

  const [frequency, setFrequency] = useState({ value: paymentDetails?.frequency ?? "weekly", error: "" });
  const [bankName, setBankName] = useState({ value: paymentDetails?.bankName ?? "", error: "" });
  const [accountNumber, setAccountNumber] = useState({ value: paymentDetails?.accountNumber ?? "", error: "" });
  const [accountName, setAccountName] = useState({ value: paymentDetails?.accountName ?? "", error: "" });

  const [isLoading, setIsLoading] = useState(false);

  const handlePaymentInfoSubmission = async () => {
    try {
      setIsLoading(true);

      // generalTextFieldValidation
      const bankNameError = generalTextFieldValidation(bankName, setBankName);
      const accountNameError = generalTextFieldValidation(accountName, setAccountName);
      const accountNumberError = generalTextFieldValidation(accountNumber, setAccountNumber);

      if (bankNameError || accountNameError || accountNumberError) {
        setIsLoading(false)
        return;
      }

      const requestModel = {
        frequency: frequency.value,
        bankName: bankName.value,
        accountNumber: accountNumber.value,
        accountName: accountName.value,
        mobileNumber
      }
      const networkRequest: any = await apiFetch.post("doctor/payment", requestModel);
      if (networkRequest.status && networkRequest?.data) {
        await updateUserDetail(networkRequest.data)
        setIsLoading(false);
        ToastAndroid.show(networkRequest.message ?? "Payment Updated Successfully", ToastAndroid.LONG);
        navigation.navigate("Home");
        return;
      }
      setIsLoading(false);
      ToastAndroid.show(networkRequest.message ?? "Payment Update Failed", ToastAndroid.LONG);
    } catch (error) {
      setIsLoading(false);
      ToastAndroid.show(error?.message ?? error?.data?.message ?? "Network Error", ToastAndroid.LONG);
    }
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Appbar
        navigation={navigation}
        back={true}
        title={`Bank Information`} />
      <ScrollView
        contentContainerStyle={{ alignItems: "center", paddingHorizontal: 20 }}
      >
        <AnicureImage
          imageSource={require("../../../assets/images/wallet_new.png")}
          desc={"wallet"}
          otherImageStyle={{ width: 31 }}
          margin={true}
        />
        <AnicureText
          otherStyles={{ width: 200, marginBottom: 40 }}
          text="Fill in your bank details that will be used for payment"
          type="subTitle"
        />

        <FormInput
          type="dropdown"
          labelName="Frequency of Payment"
          options={[
            { name: "Weekly", value: "weekly" },
            { name: "Monthly", value: "monthly" },
          ]}
          selectedValue={frequency.value}
          error={frequency.error}
          autoCapitalize="none"
          onValueChange={(text: any) => setFrequency({ value: text, error: frequency.error })}
        />

        <FormInput
          labelName="Bank Name"
          value={bankName.value}
          error={bankName.error}
          onChangeText={(text: string) => setBankName({ value: text, error: bankName.error })}
        />
        <FormInput
          labelName="Account Name"
          value={accountName.value}
          error={accountName.error}
          onChangeText={(text: string) => setAccountName({ value: text, error: accountName.error })}
        />
        <FormInput
          labelName="Account Number"
          value={accountNumber.value}
          error={accountNumber.error}
          keyboardType="numeric"
          onChangeText={(num: string) => setAccountNumber({ value: num, error: accountNumber.error })}
        />

        {(isLoading == false) ? (
          <AnicureButton
            otherStyles={{ marginTop: 10, marginBottom: 20 }}
            title="Continue"
            onPress={handlePaymentInfoSubmission}
            width={"100%"}
          />
        ) : (
          <ActivityIndicator
            size="large"
            color={APP_GREEN}
          />
        )}


      </ScrollView>

    </SafeAreaView>
  )
}

const mapStateToProps = (state: any) => {
  const { userDetail } = state.user;
  return {
    mobileNumber: userDetail.mobileNumber,
    paymentDetails: userDetail?.paymentDetails ?? {},

  }
};

export default connect(mapStateToProps, { updateUserDetail })(BankInformation)

const styles = StyleSheet.create({})
