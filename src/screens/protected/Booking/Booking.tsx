import React, { useCallback, useState } from 'react'
import { ActivityIndicator, FlatList, RefreshControl, StyleSheet, Text, ToastAndroid, View } from 'react-native'
import { connect } from 'react-redux';
import apiFetch from '../../../utils/apiFetch';
import { logError, minuteSecond, monthDayYear } from '../../../utils/helpers';
import BookingCard from '../../../components/BookingCard';
import AnicureText from '../../../components/AnicureText';
import Appbar from '../../../components/Appbar';
import { APP_GREEN } from '../../../utils/constant';
import { useFocusEffect } from '@react-navigation/native';

const Booking = ({
    navigation,
    mobileNumber,
    channelName,
    agoraToken
}: any) => {

    const [allBookings, setAllBookings] = useState([]);

    const [generalError, setGeneralError] = useState("");
    const [isLoading, setIsLoading] = useState(false);
    const [refreshing, setRefreshing] = useState(false);

    useFocusEffect(
        useCallback(() => {
            fetchAllBookings();
        }, []))

    const fetchAllBookings = async () => {
        try {
            setIsLoading(true)
            setGeneralError("Loading...")
            const networkRequest: any = await apiFetch.post("call/schedule/doctor", { mobileNumber });
            if (networkRequest.status && networkRequest.data) {
                console.log(networkRequest.data[0].paymentStatus)
                setIsLoading(false)
                setAllBookings(networkRequest.data);
                return;
            }
            logError(networkRequest, setGeneralError, setIsLoading, "No data. Pull to refresh");
        } catch (error) {
            logError(error, setGeneralError, setIsLoading, "Network Error. Pull to refresh");
        }
    }

    const handleCall = async (user: any) => {
        try {
            const requestBody = {
                recipient: user.user,
                sender: user.doctor,
                callType: "call"
            }
            const networkRequest: any = await apiFetch.post("call/create/doctor", requestBody);
            if (networkRequest.status === true) {
                navigation.navigate("VideoCall", { payload: { agoraToken, channelName } });
                return;
            }
            ToastAndroid.show(networkRequest.message ?? "User not available", ToastAndroid.LONG);
        } catch (error) {
            ToastAndroid.show(error.message ?? "User not available at the moment", ToastAndroid.LONG);
            // navigation.navigate("VideoCall", {payload: { agoraToken, channelName }} );
        }
    }
    return (
        <View style={{ flex: 1 }}>
            <Appbar
                back={true}
                navigation={navigation}
                fontSize={20}
                title="Booking"
            />
            {
                allBookings.length > 0 ?
                    <View style={{ flex: 1, paddingHorizontal: 20 }}>
                        <FlatList
                            refreshControl={
                                <RefreshControl
                                    refreshing={refreshing}
                                    onRefresh={fetchAllBookings}
                                />
                            }
                            refreshing={refreshing}
                            showsVerticalScrollIndicator={false}
                            data={allBookings}
                            renderItem={({ item, index, separators }: any) =>
                                <BookingCard
                                    farmerName={item.farmerName}
                                    status={item.status}
                                    time={minuteSecond(item.time)}
                                    farmAddress={`${item.farmAddress}`}
                                    date={monthDayYear(item.day)}
                                    symptoms={item.symptoms}
                                    onChat={() => navigation.navigate("Chat", { payload: { name: item.farmerName, sender: item.doctor, recipient: item.user } })}
                                    onView={() => navigation.navigate("ViewBooking", { payload: item })}
                                    onCall={() => handleCall(item)}
                                    key={`booking${index}`}
                                />
                            }
                            keyExtractor={(item: any, index: number) => `dfd${index}`}
                        />
                    </View>
                    :
                    <View>
                        <ActivityIndicator size="large" color={APP_GREEN} />
                        <AnicureText text={generalError} type="subTitle" />
                    </View>
            }

        </View>
    )
}

const mapStateToProps = (state: any) => {
    const { mobileNumber, agoraToken, channelName } = state.user.userDetail;
    return {
        mobileNumber,
        agoraToken,
        channelName
    }
};
export default connect(mapStateToProps)(Booking)


const styles = StyleSheet.create({})
