import React, { useState } from 'react'
import { StyleSheet, ScrollView, View, ToastAndroid } from 'react-native'
import BouncyCheckbox from 'react-native-bouncy-checkbox';
import { connect } from 'react-redux';
import AnicureText from '../../../components/AnicureText';
import Appbar from '../../../components/Appbar';
import TitleDescription from '../../../components/TitleDescription';
import commonStyling from '../../../styles/GeneralStyling';
import apiFetch from '../../../utils/apiFetch';
import { APP_GREEN } from '../../../utils/constant';
import { minuteSecond, monthDayYear } from '../../../utils/helpers';

const statusOptions = [
    { name: "Pending", value: "pending" },
    { name: "Ongoing", value: "ongoing" },
    { name: "Completed", value: "completed" },
    { name: "Cancelled", value: "cancelled" },
]

const ViewBooking = ({ navigation, mobileNumber, route }: any) => {
    const { payload } = route.params;

    const [currentStatus, setCurrentStatus] = useState(payload?.status);

    const handleChangeState =  async (item: any) => {
        try {
            
            if(payload.status === "completed" || payload.status === "cancelled") {
                ToastAndroid.show(`Sorry!!! You cannot update a ${item.value} appointment`, ToastAndroid.LONG);
                return;
            }

            ToastAndroid.show("Status Update in progress", ToastAndroid.LONG);
            const requestModel = { mobileNumber, status: item.value, scheduleId: payload._id ?? payload.id };
            const networkRequest: any = await apiFetch.post("call/schedule/status/update", requestModel);
            if (networkRequest.status) {
                setCurrentStatus(item.value)
                ToastAndroid.show(networkRequest.message ?? "Status Updated successfully", ToastAndroid.LONG);
                return;
            }
            ToastAndroid.show(networkRequest.message ?? "Status Updated failed", ToastAndroid.LONG);
        } catch (error) {
            ToastAndroid.show(error.message ?? "Status Update failed", ToastAndroid.LONG);
        }
    }

    return (
        <View style={commonStyling.flex_1}>
            <Appbar
                back={true}
                navigation={navigation}
                fontSize={20}
                title={`${payload.farmerName} Booking Details`}
            />
            <ScrollView style={[commonStyling.flex_1, { paddingHorizontal: 20 }]}>

            <View style={{ paddingHorizontal: 20, paddingBottom: 10, backgroundColor: APP_GREEN, borderRadius: 10, marginTop: 10, marginBottom: 20 }}>
                    <AnicureText left type="title" text="Appointment Status" otherStyles={{ fontSize: 15, color: "#FFFFFF", paddingTop: 7 }} />
                    <AnicureText left type="title" text="* Please Update to completed after each consultation. Operation cannot be reversed" otherStyles={{ fontSize: 7, color: "#FFFFFF", paddingTop: 0, marginBottom: 10 }} />
                    <View style={{ marginLeft: 10, flex: 1, flexWrap: "wrap", flexDirection: "row" }}>
                        {
                            statusOptions.map((item: any) => (
                                <BouncyCheckbox
                                    key={item.value}
                                    checked={item.value === currentStatus}
                                    size={15}
                                    iconStyle={{ borderWidth: 1, marginLeft: 0, marginRight: 0 }}
                                    isChecked={item.value === currentStatus}
                                    textStyle={{ color: "#FFFFFF", textDecorationLine: "none" }}
                                    fillColor={APP_GREEN}
                                    borderColor={"#FFFFFF"}
                                    text={item.name}
                                    onPress={(checked) => handleChangeState(item)}
                                />
                            ))
                        }
                    </View>
                </View>

                <TitleDescription
                    title="Appointment Type"
                    text={payload.type}
                />
                <TitleDescription
                    title="Symptoms"
                    text={payload.symptoms}
                />

                <TitleDescription
                    title="Consultation Date"
                    text={`${minuteSecond(payload.time)} ${monthDayYear(payload.day)}`}
                />
                <TitleDescription
                    title="Farm Name"
                    text={payload.farmName}
                />
                <TitleDescription
                    title="Farm Address"
                    text={payload.farmAddress}
                />
                <TitleDescription
                    title="Animal Category"
                    text={payload.animalCategory}
                />
                <TitleDescription
                    title="Age"
                    text={payload.age}
                />
                <TitleDescription
                    title="Stock Size"
                    text={payload.stockSize}
                />

                <TitleDescription
                    title="User's name"
                    text={payload.farmerName}
                />

                <TitleDescription
                    title="Doctor's Phone Number"
                    text={payload.doctor}
                />

                <TitleDescription
                    title="VetMail Customer Support Number"
                    text={"08103393894"}
                />
            </ScrollView>
        </View>
    )
}

const mapStateToProps = (state: any) => {
    return ({
        mobileNumber: state.user.userDetail.mobileNumber,
    })
}

export default connect(mapStateToProps)(ViewBooking)

const styles = StyleSheet.create({})
