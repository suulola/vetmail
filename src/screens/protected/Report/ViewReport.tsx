import React, { useEffect, useState } from 'react'
import { FlatList, RefreshControl, StyleSheet, Text, View } from 'react-native'
import { connect } from 'react-redux'
import AnicureText from '../../../components/AnicureText'
import Appbar from '../../../components/Appbar'
import ChatBox from '../../../components/ChatBox'
import commonStyling from '../../../styles/GeneralStyling'
import apiFetch from '../../../utils/apiFetch'
import { logError } from '../../../utils/helpers'

const ViewReport = ({ navigation, user }: any) => {

    const { mobileNumber } = user
    const [allReports, setAllReports] = useState([]);
    const [refreshing, setRefreshing] = useState(false);
    const [generalError, setGeneralError] = useState("");


    useEffect(() => {
        fetchAllReports();
    }, [])

    const fetchAllReports = async () => {

        try {
            setRefreshing(true);
            const networkRequest: any = await apiFetch.post("report", { doctor: mobileNumber });
            console.log(networkRequest?.data?.docs)
            if (networkRequest.status && networkRequest?.data?.docs) {
                setRefreshing(false);
                setAllReports(networkRequest.data.docs);
                return;
            }
            logError(networkRequest, setGeneralError, setRefreshing, "No data. Pull to refresh");
        } catch (error) {
            logError(error, setGeneralError, setRefreshing, "No data. Pull to refresh");
        }
    }
    return (
        <View style={commonStyling.flex_1}>
            <Appbar fontSize={20} navigation={navigation} back={true} title="Case Reports" />
            <View style={[commonStyling.flex_1, { paddingHorizontal: 10 }]}>
                {generalError.length > 0 && <AnicureText text={generalError} type="error" />}
                <FlatList
                    refreshControl={
                        <RefreshControl
                            refreshing={refreshing}
                            onRefresh={fetchAllReports}
                        />
                    }
                    refreshing={refreshing}
                    data={allReports}
                    renderItem={({ item }: any) =>
                        <ChatBox
                            leftText={item.animalCategory}
                            imageSource={require("../../../assets/images/pen.png")}
                            onPress={() => navigation.navigate("ReportDetail", { payload: item })}
                            username={`Report for ${item.clientName}`}
                            date={item.created_at}
                            lastMessage={item.remark}
                        />
                    }
                    keyExtractor={(item: any, index: number) => `chat${index}`}
                />
            </View>
        </View>

    )
}


const mapStateToProps = (state: any) => {
    return ({
        user: state.user.userDetail,
    })
}

export default connect(mapStateToProps, null)(ViewReport)

const styles = StyleSheet.create({})
