import React from 'react'
import { StyleSheet, ScrollView, View } from 'react-native'
import Appbar from '../../../components/Appbar';
import TitleDescription from '../../../components/TitleDescription';
import commonStyling from '../../../styles/GeneralStyling';
import { minuteSecondDayMonthYear } from '../../../utils/helpers';

const ReportDetails = ({ navigation, route }: any) => {
    const { payload } = route.params;

    console.log(payload);

    return (
        <View style={commonStyling.flex_1}>
            <Appbar
                back={true}
                navigation={navigation}
                fontSize={20}
                title={`${payload.clientName} Record`}
            />
            <ScrollView
                contentContainerStyle={{ paddingHorizontal: 20 }}
                showsVerticalScrollIndicator={false} >

                <TitleDescription
                    title="Animal Category"
                    text={payload.animalCategory}
                />
                <TitleDescription
                    title="Age"
                    text={payload.age}
                />
                <TitleDescription
                    title="Stock Size"
                    text={payload.stockSize}
                />
                <TitleDescription
                    title="Affected Size"
                    text={payload.affectedSize}
                />
                <TitleDescription
                    title="History"
                    text={payload.history}
                />
                <TitleDescription
                    title="Symptoms"
                    text={payload.symptoms}
                />
                <TitleDescription
                    title="Diagnosis"
                    text={payload.diagnosis}
                />
                <TitleDescription
                    title="Treatment"
                    text={payload.treatment}
                />
                <TitleDescription
                    title="Remark"
                    text={payload.remark}
                />
                <TitleDescription
                    title="Date"
                    text={minuteSecondDayMonthYear(payload.createdAt)}
                />

                <TitleDescription
                    title="VetMail Customer Support Number"
                    text={"08103393894"}
                />
            </ScrollView>
        </View>
    )
}

export default ReportDetails

const styles = StyleSheet.create({})
