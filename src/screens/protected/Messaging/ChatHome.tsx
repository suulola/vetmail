import React, { useEffect, useState } from 'react'
import { FlatList, RefreshControl, StyleSheet, Text, ToastAndroid, View } from 'react-native'
import { connect } from 'react-redux'
import Appbar from '../../../components/Appbar'
import ChatBox from '../../../components/ChatBox'
import apiFetch from '../../../utils/apiFetch'
import { logError } from '../../../utils/helpers'


const ChatHome = ({ navigation, mobileNumber }: any) => {

    const [refreshing, setRefreshing] = useState(false);
    const [allUsers, setAllUsers] = useState<any>([])
    const [generalError, setGeneralError] = useState("");

    const fetchInbox = async () => {
        try {
            setRefreshing(true);

            const requestModel = {
                sender: mobileNumber
            }
            const networkRequest: any = await apiFetch.post("chat/inbox", requestModel);
            if (networkRequest.status && networkRequest.data) {
                console.log(networkRequest.data)
                setRefreshing(false)

                setAllUsers(networkRequest.data);
                return;
            }
            logError(networkRequest, setGeneralError, setRefreshing, "No data. Pull to refresh");
        } catch (error) {
            logError(error, setGeneralError, setRefreshing, "No data. Pull to refresh");
        }
    }

    useEffect(() => {
        fetchInbox()
    }, []);

    return (
        <View style={[{ flex: 1 }]}>
            <Appbar
                navigation={navigation}
                back={true}
                title="Chats"
            />

            <View style={{ paddingTop: 20, paddingHorizontal: 20, flex: 1, }}>
                <FlatList
                    showsVerticalScrollIndicator={false}
                    refreshControl={
                        <RefreshControl
                            refreshing={refreshing}
                            onRefresh={fetchInbox}
                        />
                    }
                    refreshing={refreshing}
                    data={allUsers}
                    renderItem={({ item }: any) =>
                        <ChatBox
                            onPress={() => navigation.navigate("Chat", { payload: { name: item.fullName, sender: mobileNumber, recipient: item.sender === mobileNumber ? item.recipient : item.sender } })}
                            username={item.fullName}
                            date={item.date}
                            lastMessage={item.message}
                        />
                    }
                    keyExtractor={(item: any, index: number) => `chat${index}`}
                />
            </View>
        </View>
    )
}

const mapStateToProps = (state: any) => {
    const { mobileNumber } = state.user.userDetail;
    return {
        mobileNumber,
    }
};

export default connect(mapStateToProps)(ChatHome)


const styles = StyleSheet.create({})
