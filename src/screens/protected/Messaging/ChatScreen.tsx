import { useFocusEffect } from '@react-navigation/native'
import React, { useState, useEffect, useCallback } from 'react'
import { View, StyleSheet, Dimensions, SafeAreaView, FlatList, RefreshControl, ActivityIndicator, ToastAndroid } from 'react-native'
import { connect } from 'react-redux'
import Appbar from '../../../components/Appbar'
import ChatText from '../../../components/ChatText'
import FormInput from '../../../components/FormInput'
import ImageButton from '../../../components/ImageButton'
import apiFetch from '../../../utils/apiFetch'
import { APP_GREEN } from '../../../utils/constant'
import { logError } from '../../../utils/helpers'


const { width, height } = Dimensions.get("screen")

const ChatScreen = ({
  navigation,
  route,
  mobileNumber,
}: any) => {

  const [message, setMessage] = useState("")
  const [allMessages, setAllMessages] = useState<any>([])

  const [generalError, setGeneralError] = useState("");
  const [isLoading, setIsLoading] = useState(false);


  const { payload } = route.params;

  // for refreshing
  const [refreshing, setRefreshing] = useState(false);

  useFocusEffect(
    useCallback(() => {
      const interval = setInterval(() => handleFetchMessages(), 10000);
      return () => clearInterval(interval);
    }, []))

  const handleRefresh = async () => {
    setRefreshing(true);
    await handleFetchMessages();
    setRefreshing(false);
  }

  const handleSendMessage = async () => {
    try {
      if (!message) {
        return;
      }
      const requestModel = {
        recipient: payload.recipient,
        sender: payload.sender,
        recipientType: "user",
        message: message,
        title: ""
      }
      const networkRequest: any = await apiFetch.post("chat/create", requestModel);
      if (networkRequest.status && networkRequest.data) {
        setAllMessages([...allMessages, networkRequest.data]);
        setMessage("");
        return;
      }
      ToastAndroid.show(networkRequest.message ?? "Failed to send. Try again", ToastAndroid.LONG);
    } catch (error) {
      ToastAndroid.show(error.message ?? "Network Error. Could not send message", ToastAndroid.LONG);
    }
  }

  const handleFetchMessages = async () => {
    try {
      const requestModel = {
        recipient: payload.recipient,
        sender: payload.sender
      }
      const networkRequest: any = await apiFetch.post("chat/conversation", requestModel);
      setIsLoading(false)

      if (networkRequest.status && networkRequest.data) {
        console.log(networkRequest.data)
        setAllMessages(networkRequest.data);
        return;
      }
      ToastAndroid.show(networkRequest.message ?? "No data. Pull to refresh", ToastAndroid.LONG);
      logError(networkRequest, setGeneralError, setIsLoading, "No data. Pull to refresh");
    } catch (error) {
      setIsLoading(false)
      ToastAndroid.show(error.message ?? "Network Error. Pull to refresh", ToastAndroid.LONG);
    }
  }

  useEffect(() => {
    setIsLoading(true)
    handleFetchMessages();
  }, [])

  const flatList: any = React.useRef(null)

  return (
    <SafeAreaView style={[styles.container]}>
      <Appbar
        navigation={navigation}
        back={true}
        title={payload?.name ?? "Anon"}
      />
      <View style={{ flex: 1, marginTop: 20 }}>
        {isLoading ?
          <ActivityIndicator size="large" color={APP_GREEN} />
          :
          <FlatList
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                onRefresh={handleRefresh}
              />
            }
            refreshing={refreshing}
            data={allMessages}
            renderItem={({ item }: any) =>
              <ChatText
                key={item._id}
                time={item.updatedAt}
                text={item.message}
                position={item.sender === mobileNumber ? "right" : "left"}
              />
            }
            keyExtractor={(item: any, index: number) => `${index}`}
            ref={flatList}
            onContentSizeChange={() => flatList.current.scrollToEnd()}

          />}
      </View>

      <View style={[styles.row, { paddingHorizontal: 3, justifyContent: "center", alignItems: "center", marginHorizontal: 30 }]}>
        <FormInput
          placeholder="Type Message Here..."
          maxLength={1000}
          containerStyle={{ width: "100%", borderRadius: 100, marginTop: 20 }}
          value={message}
          onChangeText={(text: string) => setMessage(text)}
        />
        <ImageButton
          onPress={handleSendMessage}
          imageSource={require("../../../assets/images/chat_send_button.png")}
          imageStyle={{ width: 25, height: 20, marginHorizontal: 10 }}
        />
      </View>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
    // justifyContent: "space-between",
    // borderWidth: 1,
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
  },
  navBar: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: "#FFFFFF",
    paddingHorizontal: 20,
    paddingTop: 20,
    paddingBottom: 10,
  },
  backImg: {
    width: 15,
    height: 15,
    marginRight: 10,
  },
  profileImg: {
    width: 30,
    height: 30,
    borderRadius: 20,
    marginRight: 10,
  },
  recipientName: {
    fontSize: 13
  },
  actionIcons: {
    width: 25,
    height: 25,
    marginHorizontal: 7,
  }

})

const mapStateToProps = (state: any) => {
  const { mobileNumber } = state.user.userDetail;
  return {
    mobileNumber,
  }
};
export default connect(mapStateToProps)(ChatScreen)

