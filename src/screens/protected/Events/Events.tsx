import React, { useState } from 'react'
import { StyleSheet, View } from 'react-native'
import AnicureText from '../../../components/AnicureText';
import Toggle from '../../../components/Toggle'
import commonStyling from '../../../styles/GeneralStyling';
import Appbar from '../../../components/Appbar';

const Events = ({ navigation }: any) => {
    const [isPaidEvent, setIsPaidEvent] = useState(true)

    return (
        <View style={commonStyling.flex_1}>
            <Appbar
                back={true}
                navigation={navigation}
                fontSize={20}
                title="Events"
            />
            <Toggle
                onPress={() => { }}
                containerStyle={{  backgroundColor: '#FFFFFF', marginBottom: 10 }}
                titleOne="Paid"
                titleTwo="Free"
                switchState={isPaidEvent}
                setSwitchState={setIsPaidEvent}
            />
            <View style={commonStyling.centralizedContainer}>
                <AnicureText text={isPaidEvent ? 'There are no paid event/courses at the moment' : 'There are no free event/courses at the moment'} type="subTitle" />
            </View>

        </View>
    )
}

export default Events

const styles = StyleSheet.create({})
