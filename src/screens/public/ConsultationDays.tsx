import React, { useState } from 'react'
import { ActivityIndicator, SafeAreaView, ScrollView, StyleSheet, Text, View } from 'react-native'
import AnicureText from '../../components/AnicureText'
import Appbar from '../../components/Appbar'
import DateTimePicker from '@react-native-community/datetimepicker';
import AnicureButton from '../../components/AnicureButton';
import dayjs from 'dayjs';
import SuccessModal from '../../components/SuccessModal';
import { APP_GREEN } from '../../utils/constant';
import AnicureImage from '../../components/AnicureImage';
import commonStyling from '../../styles/GeneralStyling';
import { logError } from '../../utils/helpers';
import apiFetch from '../../utils/apiFetch';
import { connect } from 'react-redux';
import { updateUserDetail } from '../../store/actions/userAction'

const allDays = [
    "monday",
    "tuesday",
    "wednesday",
    "thursday",
    "friday",
    "saturday",
    "sunday",
]

const ConsultationDays = ({
    navigation,
    route,
    updateUserDetail,
    user
}: any) => {

    const { mobileNumber } = route.params;

    const [show, setShow] = useState({ value: false, day: "", startOrEnd: "" });
    const [isModalOpen, setIsModalOpen] = useState(false)

    const [isLoading, setIsLoading] = useState(false)
    const [generalError, setGeneralError] = useState("");

    const [selectedPeriod, setSelectedPeriod] = useState(user?.consultationTime ?? {
        monday: { start: "N/A", end: "N/A" },
        tuesday: { start: "N/A", end: "N/A" },
        wednesday: { start: "N/A", end: "N/A" },
        thursday: { start: "N/A", end: "N/A" },
        friday: { start: "N/A", end: "N/A" },
        saturday: { start: "N/A", end: "N/A" },
        sunday: { start: "N/A", end: "N/A" },
    });

    const navigateToIntro = () => {
        setIsModalOpen(false);
        navigation.navigate("Intro")
    }

    const handleShow = ({ value, day, startOrEnd }: any) => {
        if (startOrEnd === "clear") {
            setSelectedPeriod({
                ...selectedPeriod, [show.day]: {
                    start: "N/A",
                    end: "N/A"
                }
            })
            return;
        }
        setShow({ value, day, startOrEnd })
    }

    const handleSubmission = async () => {
        try {
            setIsLoading(true)
            const requestModel = {
                mobileNumber,
                consultationTime: selectedPeriod,
            }
            const networkRequest: any = await apiFetch.post("doctor/register/consultation", requestModel);
            if (networkRequest.status) {
                setIsLoading(false);
                if (user?.isConfirmed) {
                    console.log('object', networkRequest.data)
                    await updateUserDetail(networkRequest.data);
                    navigation.navigate("Profile")
                } else {
                    setIsModalOpen(true);
                }
                return;
            }
            logError(networkRequest, setGeneralError, setIsLoading);
        } catch (error) {
            logError(error, setGeneralError, setIsLoading);

        }
    }


    const onChange = (event: any, selectedDate: any) => {
        setShow({ value: false, day: show.day, startOrEnd: show.startOrEnd });
        const timestamp = event.nativeEvent.timestamp;
        if (!timestamp) return;
        const currentDate = dayjs(new Date(timestamp)).format('h:mmA') || "N/A";

        setSelectedPeriod({
            ...selectedPeriod, [show.day]: {
                start: show.startOrEnd === "start" ? currentDate : selectedPeriod[show.day].start,
                end: show.startOrEnd === "end" ? currentDate : selectedPeriod[show.day].end
            }
        })
    };

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <Appbar
                navigation={navigation}
                back={true}
                title={`${user?.fullName ? user.fullName : "Step 3/3"}`}
            />
            <ScrollView>
                <View style={commonStyling.registrationContainer}>

                    <AnicureImage
                        imageSource={require("../../assets/images/add_profile.png")}
                        desc={"profile"}
                        margin={true}
                    />
                    <AnicureText
                        text="Consultation Time"
                        type="subTitle"
                        otherStyles={{ color: "#1F1742", fontFamily: "Roboto-Bold", fontSize: 14 }}
                    />

                    <AnicureText
                        text="Fill in the time you will be available to talk with people in need of your services during the week. This time will be displayed on your profile"
                        type="subTitle"
                        otherStyles={{ width: "70%" }}
                    />

                    {generalError.length > 0 && <AnicureText
                        text={generalError}
                        type="error"
                    />}

                    <View style={{ marginTop: 20 }}>
                        <View style={styles.tableRow}>
                            <AnicureText text="Day" type="title" otherStyles={styles.headerText} />
                            <AnicureText text="Time" type="title" otherStyles={[styles.headerText, { width: "45%", borderRightWidth: 0, borderLeftWidth: 0 }]} />
                            <AnicureText text="Action" type="title" otherStyles={[styles.headerText, { width: "27%" }]} />
                        </View>
                        {
                            allDays.map((item: string) => (
                                <View key={item} style={styles.tableRow}>
                                    <AnicureText text={item} type="subTitle" otherStyles={styles.dayText} />
                                    <AnicureText text={`${selectedPeriod[item].start} - ${selectedPeriod[item].end}`} type="subTitle" otherStyles={[styles.dayText, { width: "45%", borderRightWidth: 0, borderLeftWidth: 0 }]} />
                                    <View style={{ width: "27%", borderWidth: 1, height: 50, justifyContent: "center", alignItems: "center", borderTopWidth: 0 }}>
                                        <AnicureButton
                                            title={`Select \n${selectedPeriod[item].start === "N/A" ? 'Start' : selectedPeriod[item].end === "N/A" ? 'End' : 'Clear'}`}
                                            onPress={() => handleShow({ value: true, day: item, startOrEnd: selectedPeriod[item].start === "N/A" ? 'start' : selectedPeriod[item].end === "N/A" ? 'end' : 'clear' })}
                                            otherStyles={{ width: 70, height: 42 }}
                                            fontSize={12}
                                        />
                                    </View>
                                </View>
                            ))
                        }
                        <View style={{ paddingHorizontal: 20 }}>
                            {(isLoading == false) ? (
                                <AnicureButton
                                    otherStyles={{ marginTop: 40 }}
                                    title="SUBMIT "
                                    onPress={handleSubmission}
                                    width={"100%"}
                                />
                            ) : (
                                    <ActivityIndicator
                                        size="large"
                                        color={APP_GREEN}
                                    />
                                )}
                        </View>
                    </View>
                </View>
            </ScrollView>

            {show.value && (
                <DateTimePicker
                    testID="dateTimePicker"
                    value={new Date()}
                    mode={"time"}
                    is24Hour={false}
                    minimumDate={new Date()}
                    display="default"
                    onChange={onChange}
                />
            )}

            <SuccessModal
                title="Registration Successful"
                description="Your request has been recorded. Currently awaiting approval to proceed. You will be notified when request is approved. You can call 08103393894 for further enquiries"
                onClose={navigateToIntro}
                actionPress={navigateToIntro}
                modalState={isModalOpen}
                actionText="Okay"
            />
        </SafeAreaView>
    )
}

const mapStateToProps = (state: any) => {
    return ({
        user: state.user.userDetail,
    })
}

export default connect(mapStateToProps, { updateUserDetail })(ConsultationDays)


const styles = StyleSheet.create({
    headerText: {
        borderWidth: 1,
        width: "21%",
        fontSize: 17,
        paddingVertical: 10
    },
    dayText: {
        fontSize: 13,
        textTransform: "capitalize",
        width: "21%",
        height: 50,
        borderTopWidth: 0,
        // textAlign: "left",
        paddingVertical: 10,
        borderWidth: 1,
        marginVertical: 0
    },
    tableRow: {
        flexDirection: "row",
        justifyContent: "center",
        width: "100%",
        // borderWidth: 1,
        marginVertical: 0,
    },

})
