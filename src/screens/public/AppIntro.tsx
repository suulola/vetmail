import React, { useState } from 'react';
import {
  StyleSheet,
  Dimensions,
  View,
  Image,
  ScrollView
} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import AnicureButton from '../../components/AnicureButton';
import AnicureText from '../../components/AnicureText';
import IntroAction from '../../components/IntroAction';
import Loader from '../../components/Loader';

const { width } = Dimensions.get('screen');

const slides = [
  {
    key: 'Consult and Get Paid',
    title: 'Consult and Get Paid',
    text: `Consult with Animal owners from the comfort of your home`,
    image: require('../../assets/images/doctor_lg.png'),
    backgroundColor: '#59b2ab',
  },
];

const AppIntro = ({ navigation }: any) => {
  const [showRealApp, setShowRealApp] = useState(false);

  const onDone = () => {
    navigation.navigate("CreateDoctor")
  };
  const handleLogin = () => {
    navigation.navigate("Login")
  };

  const renderItem = ({ item }: any) => {
    return (
      <View style={styles.slide}>
        <AnicureButton
          onPress={handleLogin}
          title={"Skip"}
          otherStyles={{ alignItems: 'flex-end', justifyContent: "flex-end" }}
          boldText={true}
          textBtn={true}
          fontSize={15}
          textColor="#1F1742"
        />
        <View style={{ justifyContent: "center", alignItems: "center", flex: 1 }}>
          <Image
            resizeMode="contain"
            style={styles.image}
            source={item.image}
          />
          <AnicureText
            text={item.title}
            type="title"
          />
          <AnicureText
            text={item.text}
            type="subTitle"
          />
        </View>
      </View>
    );
  };

  if (showRealApp) {
    return <Loader />
  } else {
    return (
      <ScrollView contentContainerStyle={{flex:1, minHeight: 600}}>
        <AppIntroSlider
          keyExtractor={(item) => item.key}
          renderItem={renderItem}
          data={slides}
          renderNextButton={() => <IntroAction onDone={onDone} handleLogin={handleLogin} />}
          bottomButton={true}
          renderDoneButton={() => <IntroAction onDone={onDone} handleLogin={handleLogin} />}
          activeDotStyle={{
            backgroundColor: "#216B36",
          }}
          dotStyle={{
            backgroundColor: "#BAB9C0"
          }}
        />
      </ScrollView>
    );
  }
};

export default AppIntro;

const styles = StyleSheet.create({
  slide: {
    width: "100%",
    height: "70%",
    alignItems: "flex-start",
    paddingHorizontal: 20,
    backgroundColor: "#F4F4F4"
  },
  image: {
    marginBottom: 10,
    borderRadius: 30,
    width: width - 100,
  },
});
