import React, { useState } from 'react'
import { ScrollView, StyleSheet, Dimensions, View, ActivityIndicator, Image, PermissionsAndroid, Platform, SafeAreaView } from 'react-native'
import AnicureButton from '../../components/AnicureButton'
import AnicureImage from '../../components/AnicureImage'
import AnicureText from '../../components/AnicureText'
import Appbar from '../../components/Appbar'
import FormInput from '../../components/FormInput'
import { APP_GREEN } from '../../utils/constant'
import commonStyling from '../../styles/GeneralStyling';
import { CameraOptions, ImageLibraryOptions, ImagePickerResponse, launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { generalTextFieldValidation } from '../../utils/validation'
import { logError } from '../../utils/helpers'
import apiFetch from '../../utils/apiFetch'
import axios from 'axios'
import { connect } from 'react-redux'
import { updateUserDetail } from '../../store/actions/userAction'

const { width, height } = Dimensions.get("screen")

const AddDoctorProfessionalDetail = ({
    navigation,
    route,
    updateUserDetail,
    user
}: any) => {

    const { mobileNumber } = route.params;

    const [title, setTitle] = useState({ value: user?.title ?? "", error: "" });
    const [qualification, setQualification] = useState({ value: user?.qualification ?? "", error: "" });
    const [registrationNumber, setRegistrationNumber] = useState({ value: user?.registrationNumber ?? "", error: "" });
    const [yearsOfExperience, setYearsOfExperience] = useState({ value: user ? `${user?.yearsOfExperience}` ?? "" : "", error: "" });
    const [clinicName, setClinicName] = useState({ value: user?.clinicName ?? "", error: "" });
    const [clinicAddress, setClinicAddress] = useState({ value: user?.clinicAddress ?? "", error: "" });
    const [bio, setBio] = useState({ value: user?.bio ?? "", error: "" });
    const [uploadedImage, setUploadedImage] = useState<any>(null);

    const [isLoading, setIsLoading] = useState(false);
    const [generalError, setGeneralError] = useState("");


    const handleSubmitProfessionalDetails = async () => {
        try {
            setIsLoading(true);
            setGeneralError("");

            // // VALIDATIONS
            const titleError = generalTextFieldValidation(title, setTitle);
            const qualificationError = generalTextFieldValidation(qualification, setQualification);
            const regNumError = generalTextFieldValidation(registrationNumber, setRegistrationNumber);
            const yearsOfExpError = generalTextFieldValidation(yearsOfExperience, setYearsOfExperience);
            const clinicNameError = generalTextFieldValidation(clinicName, setClinicName);
            const clinicAddressError = generalTextFieldValidation(clinicAddress, setClinicAddress);
            const bioError = generalTextFieldValidation(bio, setBio);


            if (titleError || qualificationError || regNumError || yearsOfExpError || clinicNameError || clinicAddressError || bioError) {
                setIsLoading(false)
                return;
            }

            console.log(uploadedImage, 'uploadedImage')

            if (user?.isConfirmed !== true) {
                if (!uploadedImage) {
                    logError({}, setGeneralError, setIsLoading, "No Image Uploaded");
                    return;
                }
            }

            // upload image first
            if (uploadedImage) {
                const imageUpload: Boolean = await uploadImageToCloud(uploadedImage);
                if (!imageUpload || imageUpload === null) return logError({}, setGeneralError, setIsLoading, "Uploading Image Failed. Please select a valid image");
            }
            //TODO: API
            const requestModel = {
                title: title.value,
                mobileNumber,
                qualification: qualification.value,
                registrationNumber: registrationNumber.value,
                clinicName: clinicName.value,
                clinicAddress: clinicAddress.value,
                bio: bio.value,
                yearsOfExperience: yearsOfExperience.value
            }

            const networkRequest: any = await apiFetch.post("doctor/register/info", requestModel);
            if (networkRequest.status) {
                setIsLoading(false);
                if (user?.isConfirmed) {
                    await updateUserDetail(networkRequest.data);
                    navigation.navigate("Profile")
                } else {
                    navigation.push("ConsultationDays", { mobileNumber: mobileNumber })
                }
                return;
            }
            logError(networkRequest, setGeneralError, setIsLoading);
        } catch (error) {
            logError(error, setGeneralError, setIsLoading);
        }
    };

    const uploadImageToCloud = async (photo: any): Promise<Boolean> => {
        try {
            var data = new FormData();
            data.append("image", {
                uri: photo.uri, //Platform.OS === "android" ? photo.uri : photo.uri.replace("file://", ""),
                type: photo.type,
                name: photo.fileName
            });
            const networkCall = await axios.post(`http://188.166.146.141/api/v1/uploads/${mobileNumber}`, data, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                },
            })
            const status: Boolean = networkCall.data.status;
            console.log(status, 'status')
            return status;

        } catch (error) {
            return false;
        }
    }

    const launchGallery = () => {
        const options: ImageLibraryOptions = {
            mediaType: "photo",
            maxWidth: 300,
            maxHeight: 300,
            quality: 0.2,
            includeBase64: true,
        }

        launchImageLibrary(options, (res: ImagePickerResponse) => {
            console.log('Response = ', res);
            if (res.didCancel) {
                console.log('User cancelled image picker');
            } else if (res.errorMessage) {
                console.log('ImagePicker Error: ', res.errorMessage);
            } else if (res.base64) {
                setUploadedImage(res)
            } else {
                console.log(JSON.stringify(res))
            }
        });
    };

    const openCamera = async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA,
                {
                    title: "App Camera Permission",
                    message: "App needs access to your camera ",
                    buttonNeutral: "Ask Me Later",
                    buttonNegative: "Cancel",
                    buttonPositive: "OK"
                }
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log("Camera permission given");
                cameraCapturing()
            } else {
                setGeneralError("Camera permission denied");
            }
        } catch (err) {
            console.log(err, 'camera error');
        }
    };

    const cameraCapturing = () => {
        const options: CameraOptions = {
            mediaType: "photo",
            maxWidth: 500,
            maxHeight: 500,
            quality: 0.7,
            saveToPhotos: true,
            includeBase64: true,
        }

        launchCamera(options, (res: ImagePickerResponse) => {
            if (res.didCancel) {
                console.log('User cancelled image picker');
            } else if (res.errorMessage) {
                console.log('ImagePicker Error: ', res.errorMessage);
            } else if (res.base64) {
                setUploadedImage(res)
            } else {
                console.log('////', res)
            }
        });
    }

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <Appbar
                navigation={navigation}
                back={true}
                title={`${user?.fullName ? user.fullName : "Step 2/3"}`}
            />
            <ScrollView>
                <View style={commonStyling.registrationContainer} >
                    <AnicureImage
                        imageSource={require("../../assets/images/doctor_thumbnail.png")}
                        desc={"profile"}
                        margin={true}
                    />
                    <AnicureText
                        text="Update Professional Record"
                        type="subTitle"
                        otherStyles={{ color: "#1F1742", fontFamily: "Roboto-Bold", fontSize: 14 }}
                    />

                    <AnicureText
                        text="Fill in your professional details"
                        type="subTitle"
                    />

                    <View style={commonStyling.cardContainer}>
                        <View style={commonStyling.registrationWhiteSheet}>

                            {generalError.length > 0 && <AnicureText
                                text={generalError}
                                type="error"
                            />}
                            <FormInput
                                labelName="Title"
                                value={title.value}
                                error={title.error}
                                autoCapitalize="none"
                                placeholder="ex Vet. Doctor"
                                onChangeText={(name: string) => setTitle({ value: name, error: title.error })}
                            />
                            <FormInput
                                labelName="Qualification"
                                value={qualification.value}
                                error={qualification.error}
                                autoCapitalize="none"
                                // placeholder="ex Vet. Doctor"
                                onChangeText={(name: string) => setQualification({ value: name, error: qualification.error })}
                            />
                            <FormInput
                                labelName="VCN Number"
                                unEditable={user?.isConfirmed ? true : false}
                                value={registrationNumber.value}
                                error={registrationNumber.error}
                                autoCapitalize="none"
                                onChangeText={(name: string) => setRegistrationNumber({ value: name, error: registrationNumber.error })}
                            />

                            <FormInput
                                labelName="Years of Experience"
                                keyboardType="numeric"
                                value={yearsOfExperience.value}
                                error={yearsOfExperience.error}
                                onChangeText={(text: string) => setYearsOfExperience({ value: text, error: yearsOfExperience.error })}
                            />

                            <FormInput
                                labelName="Company Name"
                                value={clinicName.value}
                                error={clinicName.error}
                                autoCapitalize="none"
                                onChangeText={(name: string) => setClinicName({ value: name, error: clinicName.error })}
                            />

                            <FormInput
                                labelName="Company Address"
                                textarea={true}
                                value={clinicAddress.value}
                                error={clinicAddress.error}
                                onChangeText={(text: string) => setClinicAddress({ value: text, error: clinicAddress.error })}
                            />


                            <FormInput
                                labelName="Bio"
                                textarea={true}
                                value={bio.value}
                                error={bio.error}
                                onChangeText={(text: string) => setBio({ value: text, error: bio.error })}
                            />

                            <View style={{ flexDirection: "column", justifyContent: "space-between", marginTop: 30 }}>
                                <AnicureButton
                                    icon="folder-images"
                                    iconColor="#FFFFFF"
                                    title="Upload Picture"
                                    onPress={launchGallery}
                                />
                                <AnicureText
                                    text="OR"
                                    type="title"
                                />
                                <AnicureButton
                                    icon="camera"
                                    iconColor="#FFFFFF"
                                    title="Take Picture"
                                    onPress={openCamera}
                                />
                                <View style={{ paddingVertical: 20 }}>
                                    {uploadedImage ? <Image
                                        source={{
                                            uri: `data:image/jpeg;base64,${uploadedImage.base64}`
                                        }}
                                        style={{ width: 300, height: 500, maxHeight: 500 }}
                                        resizeMode="contain"
                                    /> :
                                        user?.photo ? <Image
                                            source={{
                                                uri: user.photo
                                            }}
                                            style={{ width: 300, height: 500, maxHeight: 500 }}
                                            resizeMode="contain"
                                        /> :
                                            <AnicureText
                                                text="No Image Uploaded"
                                                type="subTitle"
                                            />
                                    }
                                </View>
                            </View>

                            {(isLoading == false) ? (
                                <AnicureButton
                                    otherStyles={{ marginTop: 40 }}
                                    title="Continue"
                                    onPress={handleSubmitProfessionalDetails}
                                    width={"100%"}
                                />
                            ) : (
                                    <ActivityIndicator
                                        size="large"
                                        color={APP_GREEN}
                                    />
                                )}
                        </View>
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    )
}

const mapStateToProps = (state: any) => {
    return ({
        user: state.user.userDetail,
    })
}

export default connect(mapStateToProps, { updateUserDetail })(AddDoctorProfessionalDetail)

const styles = StyleSheet.create({

})
