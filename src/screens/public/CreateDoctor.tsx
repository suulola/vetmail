import React, { useState } from 'react'
import { ScrollView, StyleSheet, View, ActivityIndicator, SafeAreaView } from 'react-native'
import AnicureButton from '../../components/AnicureButton'
import AnicureImage from '../../components/AnicureImage'
import AnicureText from '../../components/AnicureText'
import Appbar from '../../components/Appbar'
import FormInput from '../../components/FormInput'
import { APP_GREEN } from '../../utils/constant'
import { passwordValidation, emailValidation, confirmPasswordValidation, fullNameValidation, mobileNumberValidation, farmNameValidation } from '../../utils/validation'
import commonStyling from '../../styles/GeneralStyling';
import { stateAndLocalGovt } from '../../utils/nigeria_state';
import { logError } from '../../utils/helpers'
import apiFetch from '../../utils/apiFetch'
import { connect } from 'react-redux';
import { updateUserDetail } from '../../store/actions/userAction'

const CreateDoctor = ({ navigation, updateUserDetail, user }: any) => {

    const [mobileNumber, setMobileNumber] = useState({ value: user?.mobileNumber ?? "", error: "" });
    const [fullName, setFullName] = useState({ value: user?.fullName ?? "", error: "" });
    const [email, setEmail] = useState({ value: user?.email ?? "", error: "" });
    const [stateOfResidence, setStateOfResidence] = useState({ value: user?.location?.state ?? "", error: "" });
    const [lga, setLga] = useState({ value: user?.location?.lga ?? "", error: "" });
    const [password, setPassword] = useState({ value: "", error: "" });
    const [confirmPassword, setConfirmPassword] = useState({ value: "", error: "" });

    const [isLoading, setIsLoading] = useState(false);
    const [generalError, setGeneralError] = useState("");

    const [localGovtOptions, setLocalGovtOptions] = useState<Array<any>>([])

    const handleRegistration = async () => {
        try {
            setIsLoading(true);

            // VALIDATIONS
            const mobileNumberError = mobileNumberValidation(mobileNumber, setMobileNumber);
            const emailError = emailValidation(email, setEmail);
            const passwordError = passwordValidation(password, setPassword);
            const stateOfResidenceError = farmNameValidation(stateOfResidence, setStateOfResidence);
            const lgaError = farmNameValidation(lga, setLga);
            const confirmPasswordError = confirmPasswordValidation(password.value, confirmPassword, setConfirmPassword);
            const fullNameError = fullNameValidation(fullName, setFullName);

            if (emailError || stateOfResidenceError || lgaError || mobileNumberError || fullNameError) {
                setIsLoading(false)
                return;
            }
            if (user?.isConfirmed !== true) {
                if (passwordError || confirmPasswordError) {
                    setIsLoading(false)
                    return;
                }
            }

            const requestModel = {
                mobileNumber: mobileNumber.value,
                email: email.value,
                fullName: fullName.value,
                password: password.value,
                location: {
                    state: stateOfResidence.value,
                    lga: lga.value,
                    country: "Nigeria",
                },
            }
            const existingUserRequestModel = {
                mobileNumber: mobileNumber.value,
                email: email.value,
                fullName: fullName.value,
                location: {
                    state: stateOfResidence.value,
                    lga: lga.value,
                    country: "Nigeria",
                },
            }
            //TODO: API to register user and send OTP
            const networkRequest: any = await apiFetch.post(
                user?.isConfirmed ? "doctor/update/profile" : "doctor/create/",
                user?.isConfirmed ? existingUserRequestModel : requestModel);
            if (networkRequest.status) {
                setIsLoading(false);
                if (user?.isConfirmed) {
                    await updateUserDetail(networkRequest.data);
                    navigation.navigate("Profile")
                } else {
                    navigation.push("AddDoctorProfessionalDetail", { mobileNumber: mobileNumber.value })
                }
                return;
            }
            logError(networkRequest, setGeneralError, setIsLoading);
            setIsLoading(false)
        } catch (error) {
            logError(error, setGeneralError, setIsLoading);

        }
    };

    const getLocalGovernment = (state: string) => {

        setStateOfResidence({ value: state, error: stateOfResidence.error })
        let arr: Array<any> = [{ name: "Select Local Government of Residence", value: "" }];
        const userState = stateAndLocalGovt.filter(
            (item: any) => item.value === state,
        );
        if (userState.length > 0) {
            for (let item of userState[0].lgas) {
                arr.push({ name: item, value: item });
            }
        }
        setLocalGovtOptions(arr);
    };

    console.log(user)

    return (
        <SafeAreaView style={{ flex: 1 }}>

            <Appbar
                navigation={navigation}
                back={true}
                title={`${user?.fullName ? user.fullName : "Step 1/3"}`} />
            <ScrollView>
                <View style={commonStyling.registrationContainer} >
                    <AnicureImage
                        imageSource={require("../../assets/images/add_profile.png")}
                        desc={"profile"}
                        margin={true}
                    />
                    <AnicureText
                        text={`${user?.fullName ? "Update" : "Add"} Profile Details`}
                        type="subTitle"
                        otherStyles={{ color: "#1F1742", fontFamily: "Roboto-Bold", fontSize: 14 }}
                    />

                    <AnicureText
                        text="Fill in your personal details"
                        type="subTitle"
                    />

                    <View style={commonStyling.cardContainer}>
                        <View style={commonStyling.registrationWhiteSheet}>

                            {generalError.length > 0 && <AnicureText
                                text={generalError}
                                type="error"
                            />
                            }
                            <FormInput
                                labelName="Phone Number"
                                unEditable={user?.isConfirmed ? true : false}
                                value={mobileNumber.value}
                                error={mobileNumber.error}
                                autoCapitalize="none"
                                keyboardType="numeric"
                                onChangeText={(num: string) => setMobileNumber({ value: num, error: mobileNumber.error })}
                            />
                            
                            <FormInput
                                labelName="Full Name"
                                unEditable={user?.isConfirmed ? true : false}
                                value={fullName.value}
                                error={fullName.error}
                                autoCapitalize="none"
                                onChangeText={(name: string) => setFullName({ value: name, error: fullName.error })}
                            />

                            <FormInput
                                labelName="Email Address"
                                value={email.value}
                                error={email.error}
                                autoCapitalize="none"
                                keyboardType="email-address"
                                onChangeText={(name: string) => setEmail({ value: name, error: email.error })}
                            />
                            <FormInput
                                type="dropdown"
                                labelName="State of Residence"
                                options={stateAndLocalGovt}
                                selectedValue={stateOfResidence.value}
                                error={stateOfResidence.error}
                                autoCapitalize="none"
                                onValueChange={(text: any) => getLocalGovernment(text)}
                            />
                            <FormInput
                                type="dropdown"
                                labelName="Local Government of Residence"
                                options={localGovtOptions}
                                selectedValue={lga.value}
                                error={lga.error}
                                autoCapitalize="none"
                                onValueChange={(name: any) => setLga({ value: name, error: lga.error })}
                            />
                            {user?.isConfirmed !== true &&
                                <View>
                                    <FormInput
                                        labelName="Password"
                                        icon="password"
                                        value={password.value}
                                        error={password.error}
                                        secureTextEntry={true}
                                        onChangeText={(userPassword: string) => setPassword({ value: userPassword, error: password.error })}
                                    />

                                    <FormInput
                                        labelName="Re-enter Password"
                                        icon="password"
                                        value={confirmPassword.value}
                                        error={confirmPassword.error}
                                        secureTextEntry={true}
                                        onChangeText={(userPassword: string) => setConfirmPassword({ value: userPassword, error: confirmPassword.error })}
                                    />

                                </View>}

                            {(isLoading == false) ? (
                                <AnicureButton
                                    otherStyles={{ marginTop: 40 }}
                                    title="Continue"
                                    onPress={handleRegistration}
                                    width={"100%"}
                                />
                            ) : (
                                    <ActivityIndicator
                                        size="large"
                                        color={APP_GREEN}
                                    />
                                )}
                        </View>
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>

    )
}

const mapStateToProps = (state: any) => {
    return ({
        user: state.user.userDetail,
    })
}

export default connect(mapStateToProps, { updateUserDetail })(CreateDoctor)

const styles = StyleSheet.create({})
