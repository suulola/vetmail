import React, { useState } from 'react'
import { Image, ScrollView, StyleSheet, ToastAndroid, TouchableOpacity, View } from 'react-native'
import { connect } from 'react-redux'
import BouncyCheckbox from "react-native-bouncy-checkbox";

import AnicureText from '../../components/AnicureText'
import Appbar from '../../components/Appbar'
import Divider from '../../components/Divider'
import TitleDescription from '../../components/TitleDescription'
import { userLogout } from "../../store/actions/userAction"
import commonStyling from '../../styles/GeneralStyling'
import { APP_GREEN } from '../../utils/constant'
import apiFetch from '../../utils/apiFetch';

const profileData = [
    { title: "Edit Personal Information", imageSource: require("../../assets/images/add_profile.png"), route: "CreateDoctor", type: "buy" },
    { title: "Edit Professional Information", imageSource: require("../../assets/images/doctor_thumbnail.png"), route: "AddDoctorProfessionalDetail", type: "sell" },
    { title: "Edit Consultation Time", imageSource: require("../../assets/images/consultation_new.png"), route: "ConsultationDays", type: "chat" },
    { title: "Bank Information", imageSource: require("../../assets/images/wallet_new.png"), route: "BankInformation", type: "bank" },
]

const availabilityState = [
    { name: "Offline", value: "OFFLINE" },
    { name: "Online", value: "ONLINE" },
    { name: "Unavailable", value: "UNAVAILABLE" },
]

const ProfileScreen = ({ navigation, user, userLogout }: any) => {

    const { email, mobileNumber, availability } = user;
    const [checkBoxAvailability, setCheckBoxAvailability] = useState(availability);

    const handleSignOut = () => {
        userLogout();
    }

    const handleAvailabilitySwitch = async (item: any) => {
        try {
            setCheckBoxAvailability(item.value);
            const requestModel = { mobileNumber, status: item.value };
            const networkRequest: any = await apiFetch.post("doctor/availability", requestModel);
            if (networkRequest.status) {
                ToastAndroid.show(networkRequest.message ?? "Availability Updated Successfully", ToastAndroid.LONG);
                return;
            }
            setCheckBoxAvailability(availability);
            ToastAndroid.show(networkRequest.message ?? "Availability Update failed", ToastAndroid.LONG);
        } catch (error) {
            ToastAndroid.show(error.message ?? "Availability Update failed", ToastAndroid.LONG);
        }
    }

    return (
        <View style={[commonStyling.flex_1]}>
            <View style={styles.topContainer}>
                <Appbar
                    title="Profile"
                />

                <View style={styles.infoContainer}>
                    <TitleDescription
                        title={"Email"}
                        text={email}
                        titleStyle={styles.titleStyle}
                        textStyle={styles.textStyle}
                        containerStyle={styles.info}
                    />
                    <Divider otherStyles={styles.divider} />
                    <TitleDescription
                        title={"Phone Number"}
                        text={mobileNumber}
                        titleStyle={styles.titleStyle}
                        textStyle={styles.textStyle}
                        containerStyle={styles.info}
                    />
                </View>
                <View style={{ flexDirection: "row", paddingHorizontal: 20, paddingBottom: 10 }}>
                    <AnicureText type="title" text="Availability" otherStyles={{ fontSize: 15, color: "#FFFFFF", paddingTop: 7 }} />
                    <View style={{ marginLeft: 10, flex: 1, flexWrap: "wrap", flexDirection: "row" }}>
                        {
                            availabilityState.map((item: any) => (
                                <BouncyCheckbox
                                    key={item.value}
                                    checked={item.value === checkBoxAvailability}
                                    size={15}
                                    iconStyle={{ borderWidth: 1, marginLeft: 0, marginRight: 0 }}
                                    isChecked={item.value === checkBoxAvailability}
                                    textStyle={{ color: "#FFFFFF", textDecorationLine: "none" }}
                                    fillColor={APP_GREEN}
                                    borderColor={"#FFFFFF"}
                                    text={item.name}
                                    onPress={(checked) => handleAvailabilitySwitch(item)}
                                />
                            ))
                        }
                    </View>
                </View>
            </View>
            <View style={styles.bottomContainer}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    {profileData.map(data => (
                        <View key={data.title}>
                            <TouchableOpacity
                                onPress={() => navigation.navigate(data.route, { mobileNumber: mobileNumber })}
                                style={[commonStyling.row, { minHeight: 60, alignItems: "center" }]}
                            >
                                <Image
                                    source={data.imageSource}
                                    style={styles.profileImage}
                                    resizeMode="contain"
                                />
                                <AnicureText
                                    text={data.title}
                                    type="subTitle"
                                    otherStyles={styles.profileText}
                                />
                            </TouchableOpacity>
                            <Divider otherStyles={styles.divider2} />
                        </View>
                    ))}
                </ScrollView>
                <View>
                    <TouchableOpacity onPress={handleSignOut} style={styles.signOutContainer} >
                        <Image source={require("../../assets/images/sign_out.png")} style={styles.signOutImg} resizeMode="contain" />
                        <AnicureText text="Sign Out" type="subTitle" otherStyles={styles.signOutText} />
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    topContainer: {
        height: "40%",
        width: "100%",
        minHeight: 260,
        backgroundColor: APP_GREEN
    },
    bottomContainer: {
        height: "60%",
        width: "100%",
        paddingTop: 30,
        paddingHorizontal: 20,
        backgroundColor: '#FFFFFF',
        justifyContent: "space-between",
        paddingBottom: 40,
    },
    infoContainer: {
        paddingHorizontal: 20,
        flex: 1,
        justifyContent: "center",
    },
    titleStyle: {
        fontSize: 16,
        fontFamily: "Roboto-Bold",
        color: '#FFFFFF',
    },
    textStyle: {
        fontSize: 13,
        fontFamily: "Roboto-Regular",
        color: '#FFFFFF',
    },
    info: {
        marginBottom: 0,
    },
    divider: {
        marginBottom: 15,
        backgroundColor: '#FFFFFF',
        marginTop: 15
    },
    divider2: {
        marginBottom: 15,
        marginTop: 5,
        backgroundColor: "#B5BBC9",
    },
    profileImage: {
        width: 17,
        height: 17,
        marginRight: 20,
    },
    profileText: {
        fontSize: 16,
        color: "#757A84",
        fontFamily: "Roboto-Regular",
    },
    signOutContainer: {
        flexDirection: "row",
        alignItems: "center"
    },
    signOutImg: {
        width: 17,
        height: 18,
        marginRight: 22,
    },
    signOutText: {
        fontSize: 16,
        color: "#FB3F3F",
        fontFamily: "Roboto-Regular",
        marginBottom: 0,

    },
})

const mapStateToProps = (state: any) => {
    return ({
        user: state.user.userDetail,
    })
}

export default connect(mapStateToProps, { userLogout })(ProfileScreen);