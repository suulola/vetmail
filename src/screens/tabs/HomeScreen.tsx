/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  StyleSheet,
  Dimensions,
  View,
  ScrollView,
  Image,
  TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux';
import AnicureText from '../../components/AnicureText';
import DashboardTextHighlights from '../../components/DashboardTextHighlights';
import { userLogout } from '../../store/actions/userAction';



const dashboardSummary = [
  {
    title: "Chats",
    route: "ChatHome",
    summary: "View all chats",
    image: require("../../assets/images/chat.png"),
  },
  {
    title: "Bookings",
    route: "Booking",
    summary: "View all booking and appointment requests",
    image: require("../../assets/images/booking.png"),
  },
  {
    title: "Case Report",
    route: "ViewReport",
    summary: "View all reported cases",
    image: require("../../assets/images/record.png"),
  },
  {
    title: "Events",
    route: "Events",
    summary: "View free and paid events and courses",
    image: require("../../assets/images/events.png"),
  },

]

const { height, width } = Dimensions.get('screen');

const HomeScreen = ({ navigation, userLogout, photo, fullName }: any) => {

  return (
    <View style={styles.container}>

      {/* TOP BAR */}
      <View style={[styles.row, { justifyContent: "space-between", alignItems: "center", paddingHorizontal: 20, paddingVertical: 10 }]}>
        <View style={[styles.row, { alignItems: "center", height: 37, marginVertical: 50, }]}>
          <Image
            source={{uri: photo}}
            style={{
              width: 50,
              minHeight: 50,
              borderRadius: 50,
              marginRight: 10
            }}
            resizeMode="cover"
          />
          <View>
            <AnicureText type="title" text="Welcome" otherStyles={{ fontSize: 20, color: "#216B36", textAlign: "left" }} />
            <AnicureText type="subTitle" text={fullName} otherStyles={{ color: "#216B36", fontSize: 16 }} />
          </View>
        </View>
        <View />
      </View>
      {/* END OF TOP ROW */}
      <View style={{
        backgroundColor: "#216B36",
        flex: 1,
        borderRadius: 30
      }}>
        <View style={{
          width: "100%",
          flexDirection: "row",
          justifyContent: "space-around",
          paddingTop: 20
        }}>
          <DashboardTextHighlights
            count={0}
            description="Total Client"
          />
          <DashboardTextHighlights
            count={0}
            middle={true}
            description="Total Farm Visit"
          />
          <DashboardTextHighlights
            count={0}
            description="Total Case Report"
          />
        </View>

        <View style={{
          flex: 1,
          backgroundColor: "#F4F4F4",
          marginTop: 30,
          borderTopStartRadius: 30,
          borderTopEndRadius: 30,
          paddingTop: 20,
          paddingHorizontal: 20,
          flexDirection: "row",
          alignItems: "center",

        }}>
          <ScrollView
            showsVerticalScrollIndicator={false}
            // contentContainerStyle={{borderWidth: 2}}
          >
            <AnicureText
              type="title"
              text="Activities"
              otherStyles={{ marginTop: 10, color: "#1F1742", textAlign: "left", fontFamily: "Roboto-Medium", fontSize: 20 }}
            />

            <ScrollView
              // contentContainerStyle={{flex: 1}}
              showsHorizontalScrollIndicator={false}
              horizontal={true}>
              {dashboardSummary.map(item => (
                <TouchableOpacity
                onPress={() => {
                  // if(item.route === "chat") {
                    navigation.navigate(item.route )
                  // }
                }}
                  key={item.title}
                  style={{
                    minHeight: 151,
                    width: 150,
                    backgroundColor: "#FFFFFF",
                    marginRight: 15,
                    alignItems: "center",
                    paddingHorizontal: 20,
                    paddingVertical: 30,
                    marginTop: 20,
                    borderRadius: 10
                  }}>
                  <Image
                    source={item.image}
                    accessibilityHint={item.title}
                    style={{ width: 70, height: 70, marginBottom: 5 }}
                    resizeMode="stretch"
                  />
                  <AnicureText
                    text={`${item.title}`}
                    type="title"
                    otherStyles={{ fontSize: 14, marginVertical: 4, color: "#5F7D95"}}
                  />

                  <AnicureText
                    text={`${item.summary}`}
                    type="subTitle"
                    otherStyles={{ textAlign: "center", color: "#5F7D95" }}
                  />
                </TouchableOpacity>
              ))}
            </ScrollView>
          </ScrollView>
        </View>
      </View>
    </View>
  ); //just kind loving humble
};

const mapStateToProps = (state: any) => {
  const { photo, fullName } = state.user.userDetail;
  return {
    photo, fullName,
  }
};

export default connect(mapStateToProps, { userLogout })(HomeScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F4F4F4",
  },
  row: { flexDirection: "row" },
  topSection: {
    flex: 3,
    paddingBottom: 20,
    width: '100%',
  },

  topHeader: {
    flexDirection: 'row',
    paddingHorizontal: 20,
    marginTop: 20,
    justifyContent: 'space-between',
    alignItems: 'center',
    borderColor: 'red',
  },
});
