import React, { useState } from 'react'
import { ScrollView, StyleSheet, View, ActivityIndicator } from 'react-native'
import { connect } from 'react-redux';
import AnicureButton from '../../components/AnicureButton';
import AnicureImage from '../../components/AnicureImage';
import AnicureText from '../../components/AnicureText';
import Appbar from '../../components/Appbar';
import FormInput from '../../components/FormInput';
import SuccessModal from '../../components/SuccessModal';
import commonStyling from '../../styles/GeneralStyling';
import apiFetch from '../../utils/apiFetch';
import { APP_GREEN } from '../../utils/constant';
import { logError, minuteSecond, monthDayYear } from '../../utils/helpers';
import { farmNameValidation, farmAddressValidation, generalTextFieldValidation } from '../../utils/validation';



const Report = ({
    navigation,
    mobileNumber,
    fullName
}: any) => {

    const [clientName, setClientName] = useState({ value: "", error: "" });
    const [animalCategory, setAnimalCategory] = useState({ value: "", error: "" });
    const [age, setAge] = useState({ value: "", error: "" });
    const [stockSize, setStockSize] = useState({ value: "", error: "" });
    const [affectedSize, setAffectedSize] = useState({ value: "", error: "" });
    const [symptoms, setSymptoms] = useState({ value: "", error: "" });
    const [history, setHistory] = useState({ value: "", error: "" });
    const [diagnosis, setDiagnosis] = useState({ value: "", error: "" });
    const [treatment, setTreatment] = useState({ value: "", error: "" });
    const [remark, setRemark] = useState({ value: "", error: "" });

    const [isLoading, setIsLoading] = useState(false);

    const [isModalOpen, setIsModalOpen] = useState({ value: false, payload: "" })
    const [generalError, setGeneralError] = useState("");

    const handleCloseModal = async () => {
        setIsModalOpen({ value: false, payload: "" });
        resetFields();
    }

    const resetFields = async () => {
        setClientName({ value: "", error: "" })
        setAnimalCategory({ value: "", error: "" })
        setAge({ value: "", error: "" })
        setStockSize({ value: "", error: "" })
        setAffectedSize({ value: "", error: "" })
        setSymptoms({ value: "", error: "" })
        setHistory({ value: "", error: "" })
        setDiagnosis({ value: "", error: "" })
        setTreatment({ value: "", error: "" })
        setRemark({ value: "", error: "" })
    }

    const handleSubmitForm = async (): Promise<Boolean> => {
        try {
            setIsLoading(true);
            setGeneralError("")
            // VALIDATIONS
            const clientNameError = farmNameValidation(clientName, setClientName);
            const animalCategoryError = generalTextFieldValidation(animalCategory, setAnimalCategory);
            const ageError = generalTextFieldValidation(age, setAge);
            const stockSizeError = generalTextFieldValidation(stockSize, setStockSize);
            const symptomsError = generalTextFieldValidation(symptoms, setSymptoms);
            const affectedError = generalTextFieldValidation(affectedSize, setAffectedSize);
            const historyError = generalTextFieldValidation(history, setHistory);
            const diagnosisError = generalTextFieldValidation(diagnosis, setDiagnosis);
            const treatmentError = generalTextFieldValidation(treatment, setTreatment);
            const remarkError = generalTextFieldValidation(remark, setRemark);

            if (clientNameError || remarkError ||
                historyError || diagnosisError || treatmentError ||
                animalCategoryError || ageError ||
                stockSizeError || symptomsError
                || affectedError
            ) {
                setIsLoading(false)
                return false;
            }
            //TODO: API to register user and send OTP
            const requestModel = {
                clientName: clientName.value,
                doctor: mobileNumber,
                doctorName: fullName,
                animalCategory: animalCategory.value,
                age: age.value,
                stockSize: stockSize.value,
                affectedSize: affectedSize.value,
                symptoms: symptoms.value,
                history: history.value,
                diagnosis: diagnosis.value,
                treatment: treatment.value,
                remark: remark.value,
            }

            const networkRequest: any = await apiFetch.post("report/create", requestModel);

            if (networkRequest.status === true) {
            setIsLoading(false)
            setIsModalOpen({ value: true, payload: "" })
            return true;
            } else {
            logError(networkRequest, setGeneralError, setIsLoading);
            return false;
            }
        } catch (error) {
            logError(error, setGeneralError, setIsLoading);
            return false;
        }
    };

    return (
        <>
            <View style={{ flex: 1, paddingTop: 20 }}>
                <Appbar title="Create Case Report" fontSize={20} />
                <ScrollView contentContainerStyle={{ paddingTop: 20 }}>
                    <View style={commonStyling.registrationContainer} >
                        <AnicureImage
                            imageSource={require("../../assets/images/doctor_thumbnail.png")}
                            desc={"profile"}
                            margin={true}
                        />

                        <AnicureText
                            text="Fill in the necessary information"
                            type="subTitle"
                        />

                        <View style={commonStyling.cardContainer}>
                            <View style={commonStyling.registrationWhiteSheet}>

                                <AnicureText
                                    text={generalError}
                                    type="error"
                                />

                                <FormInput
                                    labelName="Client Name"
                                    value={clientName.value}
                                    error={clientName.error}
                                    autoCapitalize="none"
                                    onChangeText={(name: string) => setClientName({ value: name, error: clientName.error })}
                                />

                                <FormInput
                                    type="dropdown"
                                    options={[
                                        { name: "Select Category", value: "" },
                                        { name: "Layers", value: "Layers" },
                                        { name: "Broilers", value: "Broilers" },
                                        { name: "Turkey", value: "Turkey" },
                                        { name: "Catfish", value: "Catfish" },
                                        { name: "Tilapia", value: "Tilapia" },
                                        { name: "Cattle", value: "Cattle" },
                                        { name: "Pets", value: "Pets" },
                                        { name: "Goat/Sheep", value: "Goat/Sheep" },
                                        { name: "Pig", value: "Pig" },
                                        { name: "Others", value: "Others" },
                                    ]}
                                    labelName="Animal Category"
                                    selectedValue={animalCategory.value}
                                    error={animalCategory.error}
                                    onValueChange={(text: string) => setAnimalCategory({ value: text, error: animalCategory.error })}
                                />
                                <FormInput
                                    labelName="Age of Animals(Weeks)"
                                    value={age.value}
                                    error={age.error}
                                    keyboardType="numeric"
                                    onChangeText={(text: string) => setAge({ value: text, error: age.error })}
                                />

                                <FormInput
                                    labelName="Stock Size"
                                    value={stockSize.value}
                                    error={stockSize.error}
                                    keyboardType="numeric"
                                    onChangeText={(text: string) => setStockSize({ value: text, error: stockSize.error })}
                                />

                                <FormInput
                                    labelName="Number of Mortality"
                                    value={affectedSize.value}
                                    error={affectedSize.error}
                                    keyboardType="numeric"
                                    onChangeText={(text: string) => setAffectedSize({ value: text, error: affectedSize.error })}
                                />

                                <FormInput
                                    textarea
                                    labelName="Symptoms"
                                    value={symptoms.value}
                                    error={symptoms.error}
                                    onChangeText={(text: string) => setSymptoms({ value: text, error: symptoms.error })}
                                />
                                <FormInput
                                    textarea
                                    labelName="History"
                                    value={history.value}
                                    error={history.error}
                                    onChangeText={(text: string) => setHistory({ value: text, error: history.error })}
                                />
                                <FormInput
                                    textarea
                                    labelName="Diagnosis"
                                    value={diagnosis.value}
                                    error={diagnosis.error}
                                    onChangeText={(text: string) => setDiagnosis({ value: text, error: diagnosis.error })}
                                />
                                <FormInput
                                    textarea
                                    labelName="Treatment"
                                    value={treatment.value}
                                    error={treatment.error}
                                    onChangeText={(text: string) => setTreatment({ value: text, error: treatment.error })}
                                />
                                <FormInput
                                    textarea
                                    labelName="Remark"
                                    value={remark.value}
                                    error={remark.error}
                                    onChangeText={(text: string) => setRemark({ value: text, error: remark.error })}
                                />

                                {(isLoading == false) ? (
                                    <AnicureButton
                                        title="Submit"
                                        onPress={handleSubmitForm}
                                    />
                                ) : (
                                        <ActivityIndicator
                                            size="large"
                                            color={APP_GREEN}
                                        />
                                    )}
                            </View>
                        </View>
                        <SuccessModal
                            title="Report Submitted"
                            description={`Case Report for ${clientName.value} has been submitted`}
                            onClose={handleCloseModal}
                            actionPress={handleCloseModal}
                            modalState={isModalOpen.value}
                            actionText={"Okay"}
                        />
                    </View>
                </ScrollView>

            </View>
        </>
    )

}

const mapStateToProps = (state: any) => {
    const { mobileNumber, fullName } = state.user.userDetail;
    return {
        mobileNumber, fullName
    }
};

export default connect(mapStateToProps)(Report);

const styles = StyleSheet.create({

})
