import {createStore, combineReducers, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import userReducer from './reducer/userReducer';
import errorReducer from './reducer/errorReducer';
import vaccineReducer from './reducer/vaccineReducer';

const middlewares = [ReduxThunk];

const rootReducer = combineReducers({
  user: userReducer,
  vaccine: vaccineReducer,
  error: errorReducer
});


export default () => {
  const store = createStore(
    rootReducer,
    applyMiddleware(...middlewares)
  );
  return { store };
};

