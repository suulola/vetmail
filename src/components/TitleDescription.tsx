import React from 'react'
import { StyleProp, StyleSheet, Text, TextStyle, View, ViewStyle } from 'react-native'
import generalStyling from '../styles/GeneralStyling'
import AnicureText from './AnicureText'

interface ITitleDescription {
    title: string,
    subText?: string,
    text: string,
    titleStyle?: StyleProp<TextStyle>, 
    textStyle?: StyleProp<TextStyle>, 
    containerStyle?: StyleProp<ViewStyle>, 
}

const TitleDescription = ({
    title,
    subText,
    text,
    titleStyle,
    textStyle,
    containerStyle,
}: ITitleDescription) => {
    return (
        <View style={[styles.container, containerStyle]}>
            <AnicureText
                text={title}
                type="title"
                otherStyles={[styles.smallerTitle, titleStyle]}
            />
            <View style={generalStyling.row}>
                <AnicureText
                    text={text}
                    type="subTitle"
                    otherStyles={[styles.boldText, textStyle]}
                />
                <AnicureText
                    text={subText ?? ''}
                    type="subTitle"
                    otherStyles={[styles.smallerTitle]}
                />
            </View>
        </View>
    )
}

export default TitleDescription

const styles = StyleSheet.create({
    container: {
        marginBottom: 20,
    },
    smallerTitle: {
        // color: "#B5BBC9",
        fontSize: 13,
        textAlign: "left"
    },
    boldText: {
        fontSize: 16,
        textTransform: "capitalize",
        color: "#757A84",
        textAlign: "left",
        marginRight: 5
    },
})
