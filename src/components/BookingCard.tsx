import React from 'react'
import { Image, StyleSheet, Dimensions, View, StyleProp, ViewStyle } from 'react-native'
import AnicureButton from './AnicureButton'
import AnicureText from './AnicureText'
import Divider from './Divider'
import { ImageTextRow } from './ImageTextRow'

const { width } = Dimensions.get("screen")

interface IUpcoming {
    onView: () => void,
    onChat: () => void,
    onCall?: () => void,
    farmerName: string,
    farmAddress: string,
    symptoms: string,
    date: string,
    time: string,
    status: string,
    otherStyles?: StyleProp<ViewStyle>
}

const BookingCard = ({
    onChat,
    onCall,
    onView,
    farmerName,
    farmAddress,
    symptoms,
    date,
    time,
    otherStyles,
    status,
}: IUpcoming) => {
    return (
        <View style={[{ width: "100%", marginTop: 20, backgroundColor: "#FFFFFF", borderRadius: 20, padding: 10 }, otherStyles]}>
            <View style={[styles.row, { marginBottom: 0 }]}>
                <Image
                    source={require("../assets/images/add_profile.png")}
                    style={{ width: 75, height: 75, borderRadius: 10, marginRight: 10,  }}
                    resizeMode="contain"
                />
                <View>
                    <AnicureText
                        text={farmerName}
                        type="title"
                        otherStyles={{ color: "#1F1742", fontSize: 20, textAlign: "left" }}
                    />
                    <AnicureText
                        text={farmAddress}
                        type="title"
                        otherStyles={{ color: "#216B36", fontSize: 15, textAlign: "left" }}
                    />

                    <AnicureText
                        text={symptoms}
                        type="subTitle"
                        left
                        otherStyles={{ marginVertical: 5, color: "#ADADAD", fontSize: 7, fontFamily: "Roboto-Bold", width: width / 1.8 }}
                    />
                    <AnicureText
                        text={status.toUpperCase()}
                        type="title"
                        otherStyles={{ color: "#216B36", fontSize: 13, textAlign: "left" }}
                    />
                </View>
            </View>
            <Divider noSpace />
            <View style={{ flexDirection: "row" }}>
                <ImageTextRow status="calender" type="large" text={date} boldText />
                <ImageTextRow status="time" type="large" text={time} boldText />
            </View>
            {(onCall && onChat) && <View style={styles.rowBetween}>
                <AnicureButton
                    title="Call" 
                    textBtn
                    width={width / 4}
                    callBtn
                    height={40}
                    onPress={onCall}
                />
                <AnicureButton
                    title="Chat"
                    width={width / 4}
                    height={40}
                    onPress={onChat}
                />
                <AnicureButton
                    title="View" 
                    textBtn
                    width={width / 4}
                    height={40}
                    callBtn
                    onPress={onView}
                />
                
            </View>}
        </View>
    )
}

export default BookingCard

const styles = StyleSheet.create({
    row: {
        flexDirection: "row",
    },
    rowBetween: {
        flexDirection: "row",
        justifyContent: "space-between",
        paddingTop: 5,
    }
})
