import React from 'react'
import { StyleSheet, Dimensions, View } from 'react-native'
import AnicureText from './AnicureText';
import { minuteSecond, minuteSecondDay } from '../utils/helpers';

const { width, height } = Dimensions.get("screen")

type PositionTypes = 'left' | 'right';
interface IChatText {
    text: string,
    position: PositionTypes,
    time: string,
}

const ChatText = ({ text, position, time }: IChatText) => {
    return (
        <View style={{
            // borderWidth: 1,
            marginLeft: position === "right" ? "20%" : 0,
            paddingLeft: position === "right" ? 20 : 0,
            marginRight: position === "left" ? "20%" : 0,
            paddingRight: position === "left" ? 20 : 0,
            alignItems: position === "right" ? "flex-end" : "flex-start",
        }}>
            <AnicureText
                left
                otherStyles={{
                    fontFamily: "Roboto-Regular",
                    backgroundColor: "#F9FFFA",
                    // borderWidth: 1,
                    borderRadius: 20,
                    fontSize: 15,
                    color: "#757A84",
                    paddingHorizontal: 20,
                    paddingVertical: 10,
                    marginBottom: 0,
                    // width: (2 / 3 * width),
                }}
                text={text} type="subTitle" />
            <AnicureText
                left
                otherStyles={{
                    fontFamily: "Roboto-Italic",
                    // borderWidth: 1,
                    fontSize: 11,
                    color: "#B5BBC9",
                    marginTop: 0,
                    marginLeft: 20,
                    marginRight: 10,
                    maxWidth: 100,
                }}
                text={minuteSecondDay(time)} type="subTitle" />

        </View>
    )
}

export default ChatText

const styles = StyleSheet.create({})
