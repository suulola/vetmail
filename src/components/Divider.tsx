import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const Divider = ({ otherStyles }: any) => {
    return (
        <View
            style={[{
                width: "100%",
                height: 1,
                marginTop: 10,
                marginBottom: 7,
                backgroundColor: "#707070",
                opacity: 0.3
            }, otherStyles]}
        />
    )
}

export default Divider

const styles = StyleSheet.create({})
