import React from 'react'
import { Image, TouchableOpacity, ImageSourcePropType, StyleSheet, Text, View } from 'react-native'
// import { monthDay } from '../utils/helpers'
import Divider from './Divider'
import AnicureText from './AnicureText';
import { APP_GREEN } from '../utils/constant'
import { minuteSecondDay, minuteSecondDayMonthYear } from '../utils/helpers';

interface IChatBox {
    imageSource?: ImageSourcePropType,
    leftText?: string,
    username: string,
    date: string,
    onPress: () => void,
    lastMessage: string,
}

const ChatBox = ({
    imageSource,
    username,
    leftText,
    date,
    onPress,
    lastMessage
}: IChatBox) => {
    return (
        <TouchableOpacity
            onPress={onPress}
        >
            <View style={[{ flexDirection: 'row', justifyContent: 'space-between', alignItems: "flex-start" }]}>
                <View>
                    {imageSource ? <Image source={imageSource} style={styles.image} resizeMode="contain" /> : <View style={[styles.image, styles.initialsContainer]}><AnicureText otherStyles={[styles.initialsText]} type="subTitle" text={username.slice(0, 1)} /></View>}
                </View>
                <View style={[{ flex: 1, marginHorizontal: leftText ? 10 : 20 }]}>
                    <AnicureText left text={username} type="subTitle" otherStyles={styles.username} />
                    <AnicureText left text={lastMessage} type="subTitle" otherStyles={styles.lastMessage} />
                </View>
                <AnicureText
                    type="subTitle"
                    text={leftText ? leftText : minuteSecondDayMonthYear(date)}
                    otherStyles={styles.date}
                />
            </View>
            <Divider otherStyles={styles.divider} />
        </TouchableOpacity>
    )
}

export default ChatBox

const styles = StyleSheet.create({
    image: {
        width: 42,
        height: 42,
        borderRadius: 100,
    },
    initialsText: {
        fontSize: 16,
        textTransform: "uppercase",
        color: "#FFFFFF",
    },
    initialsContainer: {
        backgroundColor: APP_GREEN,
        justifyContent: "center",
        alignItems: "center",
    },
    username: {
        fontSize: 16,
        fontFamily: "Roboto-Bold",
        color: "#757A84",
        textTransform: "capitalize",
        marginTop: 0
    },
    lastMessage: {
        fontSize: 13,
        fontFamily: "Roboto-Regular",
        color: "#757A84",
        marginTop: 0

    },
    date: {
        fontSize: 11,
        fontFamily: "Roboto-Regular",
        color: "#757A84",
    },
    divider: {
        backgroundColor: "#B5BBC9",
        marginBottom: 20,
        marginTop: 15
    }
})
